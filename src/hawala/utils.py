"""Define utility functions for Hawala."""

import json
import os
import sys
from functools import wraps
from itertools import starmap
from pathlib import Path
from typing import Any, Callable, Dict, MutableMapping, Tuple

import jsonschema
import toml
import typer
from jsonschema.exceptions import SchemaError, ValidationError

from .exceptions import HawalaConfValidationError
from .settings import CONF_JSON_SCHEMA, DATA_DIR_PATH, DB_PATH


def validate_hawala_toml(toml_path: Path) -> MutableMapping[str, Any]:
    """Validate a TOML file against hawala schema.

    Parameters
    ----------
    toml_path : Path
        The path to the TOML file.

    Returns
    -------
    dict with str as keys and Any as values
        The dictionary containing the validated config.

    Raises
    ------
    HawalaConfValidationError
        If TOML or JSON schema invalidation occurs.

    """
    try:
        config = toml.load(toml_path)
    except (TypeError, FileNotFoundError, toml.TomlDecodeError) as err:
        raise HawalaConfValidationError(str(err)) from err
    else:
        # Read JSON schema
        with (Path(__file__).parent.absolute() / CONF_JSON_SCHEMA).open() as path:
            json_schema = json.load(path)

        # Validate schema
        try:
            jsonschema.validate(config, json_schema)
            return config
        except (ValidationError, SchemaError) as err:
            raise HawalaConfValidationError(str(err)) from err


def is_hawala_project(func: Callable) -> Callable:
    """Decorator function to check if the project is initialized.

    Parameters
    ----------
    func : callable
        The function to be wrapped.

    Returns
    -------
    callable
        The wrapped function.

    Raises
    ------
    typer.Exit
        If the project is not a hawala project.

    """

    @wraps(func)
    def decorated_function(*args, **kwargs):
        # Verify if `init` has been run or not
        if not DATA_DIR_PATH.exists() or not DB_PATH.exists():
            typer.secho(
                "This is not a hawala project yet. "
                "Try running `hawala init` before proceeding.",
                fg=typer.colors.YELLOW,
                bold=True,
            )
            raise typer.Exit(code=1)

        return func(*args, **kwargs)

    return decorated_function


def check_valid_paths(paths: Dict[str, str]) -> Dict[str, Tuple[str, bool]]:
    """Verify if `paths` are valid paths.

    Parameters
    ----------
    paths : dict with str as keys and str as values
        The {name: path} pairs to check.

    Returns
    -------
    dict with str as keys and tuple of Path and bool as values
        A dictionary containing validity of the paths provided.

    """
    delimiter = ":"  # Default delimiter for splitting PATH listing
    if sys.platform.startswith("linux") or sys.platform.startswith("darwin"):
        delimiter = ":"
    elif sys.platform.startswith("win32") or sys.platform.startswith("cygwin"):
        delimiter = ";"

    _PATH_paths = os.environ["PATH"].split(delimiter)  # individual paths in PATH
    _PATH_dir_paths = [
        path for path in _PATH_paths if Path(path).is_dir()
    ]  # directories in PATH
    _PATH_dirs_listings = {
        str(child_path.stem): str(child_path)
        for dir_path in _PATH_dir_paths
        for child_path in Path(dir_path).iterdir()
    }  # directory listings

    def _format_path(path: str) -> Tuple[str, bool]:
        """Format `path` based on whether path is present in PATH or not."""
        if Path(path).resolve().exists():
            return str(path), True
        elif path in _PATH_dirs_listings:
            return _PATH_dirs_listings[path], True
        else:
            return path, False

    # Check for path existence normally and if invalid search within PATH listing
    verified_paths_report = {name: _format_path(path) for name, path in paths.items()}

    return verified_paths_report


def verify_config_integrity(
    config_file: Path,
) -> Tuple[MutableMapping[str, Any], Dict[str, bool], bool]:
    """Verify the integrity of the `config_file`.

    Parameters
    ----------
    config_file : Path
        The path of the config file whose integrity is to be checked.

    Returns
    -------
    tuple of dict with str as keys and Any as values,
    dict with str as keys and tuple of str and bool as values and bool
        The dictionary containing the verified config,
        dictionary containing the verified paths and a boolean having
        True if all paths in the `config_file` are valid.

    Raises
    ------
    typer.Exit
        If the config file is invalid.

    """
    try:
        # Verify config file sanity
        config = validate_hawala_toml(config_file)
        typer.secho(
            f"Syntax and schema   : {config_file} ... OK",
            fg=typer.colors.GREEN,
            bold=True,
        )
    except HawalaConfValidationError as err:
        typer.secho(
            f"Syntax and schema   : {config_file} ... FAIL",
            fg=typer.colors.RED,
            bold=True,
        )
        typer.echo(err)
        raise typer.Exit(code=1)
    else:
        # Verify all paths specified in config are accessible
        # Mandatory config
        redis_config = config["redis"]
        pyabc_config = config["pyabc"]
        unverified_paths = {
            "Redis server": redis_config["server_path"],
            "Redis configuration": redis_config["conf_path"],
            "Python binary": pyabc_config["python_bin"],
            "Simulation script": pyabc_config["python_script"],
            "Model": pyabc_config["model_path"],
            "Morpheus binary": pyabc_config["morpheus_bin"],
        }
        # Optional config
        local_config = config.get("local")
        if local_config:
            unverified_paths.update(
                {
                    "Redis CLI": local_config["redis_cli_path"],
                }
            )
        # Config paths check
        verified_paths = check_valid_paths(unverified_paths)
        for name, (path, valid) in verified_paths.items():
            typer.secho(
                f"{name: <20}: {path} ... {'OK' if valid else 'FAIL'}",
                fg=typer.colors.GREEN if valid else typer.colors.RED,
                bold=True,
            )

        all_valid = all(starmap(lambda _, x: x, verified_paths.values()))
        return (config, verified_paths, all_valid)
