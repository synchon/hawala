"""Define database tables for storage."""

import databases
import orm
import sqlalchemy

from ..settings import DB_PATH

database = databases.Database(f"sqlite:///{str(DB_PATH)}")
metadata = sqlalchemy.MetaData()


class SLURMRun(orm.Model):
    __tablename__ = "slurm_runs"
    __database__ = database
    __metadata__ = metadata

    id = orm.Integer(primary_key=True)
    job_name = orm.String(max_length=100)
    account = orm.String(max_length=100)
    wall_time = orm.String(max_length=20)
    email_id = orm.String(max_length=100)
    mail_type = orm.String(max_length=20)
    partition = orm.String(max_length=20)
    ntasks = orm.Integer()
    cpus_per_task = orm.Integer()
    submission_date = orm.Date()
    submission_time = orm.Time()
    job_id = orm.Integer(index=True)


class LocalRun(orm.Model):
    __tablename__ = "local_runs"
    __database__ = database
    __metadata__ = metadata

    id = orm.Integer(primary_key=True)
    job_name = orm.String(max_length=100)
    os = orm.String(max_length=7)
    cpus = orm.Integer()
    time = orm.String(max_length=20)
    submission_date = orm.Date()
    submission_time = orm.Time()
