"""Define abstract base class for context adapter."""

from abc import ABC, abstractmethod


class AbstractContextAdapter(ABC):
    """
    Abstract base class for context adapter.

    For every interface that is required, one needs to provide a concrete
    implementation of this abstract class.

    """

    @abstractmethod
    def to_string(self) -> str:
        raise NotImplementedError("Derived classes need to implement this.")
