"""Define local sub-commands for Hawala."""

import asyncio
from datetime import datetime
from pathlib import Path
from sqlite3 import OperationalError

import typer
from plumbum import NOHUP, local
from plumbum.commands import CommandNotFound
from tabulate import tabulate

from ..db import LocalRun
from ..settings import DATA_DIR_PATH
from ..utils import is_hawala_project, verify_config_integrity
from .local_payload import LocalPayload

app = typer.Typer()


@app.callback()
def callback():
    """
    Manage local script generation and submission.
    """


@app.command()
@is_hawala_project
def submit(
    config_file: Path = typer.Argument(
        ...,
        help="TOML config file for hawala.",
    ),
):
    """
    Submit a local job.
    """
    # Validate config file
    config, _, _ = verify_config_integrity(config_file)
    # Extract data from validated config
    local_config = config.get("local")
    if not local_config:
        typer.secho(
            f"[local] section was not found in {config_file}.",
            fg=typer.colors.RED,
            bold=True,
        )
        raise typer.Exit(code=1)

    redis_config = config["redis"]
    pyabc_config = config["pyabc"]

    local_payload = LocalPayload(
        job_name=local_config["job_name"],
        os=local_config["os"],
        time=local_config["time"],
        redis_server_path=redis_config["server_path"],
        redis_conf_path=redis_config["conf_path"],
        redis_password=redis_config["password"],
        redis_cli_path=local_config["redis_cli_path"],
        python_path=pyabc_config["python_bin"],
        python_script_path=pyabc_config["python_script"],
        model_path=pyabc_config["model_path"],
        morpheus_path=pyabc_config["morpheus_bin"],
        model_db_path=pyabc_config["database_path"],
        cpus=local_config.get("cpus"),
    )

    # Display generated job script to stdout
    typer.echo(local_payload.to_string())

    # Ask for confirmation of submission
    submit_confirm = typer.confirm("Do you want to submit it?")

    # Submission denied
    if not submit_confirm:
        typer.secho("Submission was aborted!", fg=typer.colors.RED, bold=True)
        raise typer.Exit(code=1)

    # Submit confirmed
    try:
        bash = local["bash"]
    except CommandNotFound:
        typer.secho(
            "bash was not found in PATH.",
            fg=typer.colors.RED,
            bold=True,
        )
        raise typer.Exit(code=1)
    else:
        submission_date = datetime.now().date()
        submission_time = datetime.now().time()
        # Write file in cache for reference
        job_script_path = (
            DATA_DIR_PATH / f"{local_payload.job_name}_"
            f"{submission_date}_"
            f"{submission_time}.sh"
        )
        # Create file
        job_script_path.touch()
        # Write contents to file
        try:
            job_script_path.write_text(
                local_payload.to_string(), encoding="utf-8", errors="strict"
            )
            typer.secho(
                f"Job script successfully created at: {job_script_path}",
                fg=typer.colors.GREEN,
                bold=True,
            )
        except ValueError as err:
            typer.secho("Job script creation failed.", fg=typer.colors.RED, bold=True)
            typer.echo(err)
            raise typer.Exit(code=1)
        else:
            # Submit job script
            _ = bash[str(job_script_path)] & NOHUP(
                stdout=f"{local_payload.job_name}.out",
                stderr=f"{local_payload.job_name}.err",
            )
            # Create entry in database
            asyncio.run(
                LocalRun.objects.create(
                    job_name=local_payload.job_name,
                    os=local_payload.os,
                    cpus=local_payload.cpus,
                    time=local_payload.time,
                    submission_date=submission_date,
                    submission_time=submission_time,
                )
            )
            raise typer.Exit()


@app.command()
def submissions():
    """
    Retrieve successful local submissions.
    """
    try:
        runs = asyncio.run(LocalRun.objects.all())
        entries = [
            [
                row.job_name,
                row.os,
                row.cpus,
                row.time,
                row.submission_date,
                row.submission_time,
            ]
            for row in runs
        ]
        headers = [
            "Job Name",
            "OS",
            "CPUs",
            "Time Allotted",
            "Submitted On",
            "Submitted At",
        ]
        typer.secho(
            tabulate(entries, headers, tablefmt="pretty"), fg=typer.colors.MAGENTA
        )
        raise typer.Exit()
    except OperationalError as err:
        typer.secho("Database operation error.", fg=typer.colors.RED, bold=True)
        typer.echo(err)
        raise typer.Exit(code=1)
