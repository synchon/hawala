"""Settings for Hawala."""

from pathlib import Path

# DB_PATH is the absolute path for the storage file
DB_PATH = Path(".").resolve() / ".hawala.db"

# DATA_DIR is the directory for generated scripts
DATA_DIR_PATH = Path(".").resolve() / "scripts"

# CONF_JSON_SCHEMA is the configuration JSON schema file
CONF_JSON_SCHEMA = "conf_schema.json"
