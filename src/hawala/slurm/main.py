"""Define slurm sub-commands for Hawala."""

import asyncio
from datetime import datetime
from pathlib import Path
from sqlite3 import OperationalError

import typer
from plumbum import local
from plumbum.commands import CommandNotFound
from tabulate import tabulate

from ..db import SLURMRun
from ..settings import DATA_DIR_PATH
from ..utils import is_hawala_project, verify_config_integrity
from .slurm_payload import SLURMPayload

app = typer.Typer()


@app.callback()
def callback():
    """
    Manage SLURM script generation and submission.
    """


@app.command()
@is_hawala_project
def submit(
    config_file: Path = typer.Argument(
        ...,
        help="TOML config file for hawala.",
    ),
):
    """
    Submit a SLURM job.
    """
    # Validate config file
    config, _, _ = verify_config_integrity(config_file)
    # Extract data from validated config
    slurm_config = config.get("slurm")
    if not slurm_config:
        typer.secho(
            f"[slurm] section was not found in {config_file}.",
            fg=typer.colors.RED,
            bold=True,
        )
        raise typer.Exit(code=1)

    redis_config = config["redis"]
    pyabc_config = config["pyabc"]

    slurm_payload = SLURMPayload(
        job_name=slurm_config["job_name"],
        account=slurm_config["account"],
        time=slurm_config["time"],
        email_id=slurm_config["email_id"],
        redis_server_path=redis_config["server_path"],
        redis_conf_path=redis_config["conf_path"],
        redis_password=redis_config["password"],
        python_path=pyabc_config["python_bin"],
        python_script_path=pyabc_config["python_script"],
        model_path=pyabc_config["model_path"],
        morpheus_path=pyabc_config["morpheus_bin"],
        model_db_path=pyabc_config["database_path"],
        mail_type=slurm_config.get("mail_type"),
        partition=slurm_config["partition"],
        ntasks=slurm_config.get("ntasks"),
        cpus_per_task=slurm_config.get("cpus_per_task"),
        mem_per_cpu=slurm_config.get("mem_per_cpu"),
        output_file=slurm_config.get("output_file"),
        error_file=slurm_config.get("error_file"),
    )

    # Display generated job script to stdout
    typer.echo(slurm_payload.to_string())

    # Ask for confirmation of submission
    submit_confirm = typer.confirm("Do you want to submit it?")

    # Submission denied
    if not submit_confirm:
        typer.secho("Submission was aborted!", fg=typer.colors.RED, bold=True)
        raise typer.Exit(code=1)

    # Submit confirmed
    try:
        sbatch = local["sbatch"]
    except CommandNotFound:
        typer.secho(
            "sbatch was not found in PATH.",
            fg=typer.colors.RED,
            bold=True,
        )
        raise typer.Exit(code=1)
    else:
        submission_date = datetime.now().date()
        submission_time = datetime.now().time()
        # Write file in cache for reference
        job_script_path = (
            DATA_DIR_PATH / f"{slurm_payload.job_name}_"
            f"{submission_date}_"
            f"{submission_time}.sh"
        )
        # Create file
        job_script_path.touch()
        # Write contents to file
        try:
            job_script_path.write_text(
                slurm_payload.to_string(), encoding="utf-8", errors="strict"
            )
            typer.secho(
                f"Job script successfully created at: {job_script_path}",
                fg=typer.colors.GREEN,
                bold=True,
            )
        except ValueError as err:
            typer.secho("Job script creation failed.", fg=typer.colors.RED, bold=True)
            typer.echo(err)
            raise typer.Exit(code=1)
        else:
            # Submit job script
            sbatch_output = sbatch.run(["--parsable", str(job_script_path)])
            sbatch_stdout = sbatch_output[1]
            job_id = sbatch_stdout.split(";")[0]
            # Create entry in database
            asyncio.run(
                SLURMRun.objects.create(
                    job_name=slurm_payload.job_name,
                    account=slurm_payload.account,
                    wall_time=slurm_payload.time,
                    email_id=slurm_payload.email_id,
                    mail_type=slurm_payload.mail_type,
                    partition=slurm_payload.partition,
                    ntasks=slurm_payload.ntasks,
                    cpus_per_task=slurm_payload.cpus_per_task,
                    submission_date=submission_date,
                    submission_time=submission_time,
                    job_id=job_id,
                )
            )
            raise typer.Exit()


@app.command()
def submissions():
    """
    Retrieve successful SLURM submissions.
    """
    try:
        runs = asyncio.run(SLURMRun.objects.all())
        entries = [
            [
                row.job_id,
                row.account,
                row.job_name,
                row.email_id,
                row.submission_date,
                row.submission_time,
            ]
            for row in runs
        ]
        headers = [
            "Job ID",
            "Account",
            "Job Name",
            "Email ID",
            "Submitted On",
            "Submitted At",
        ]
        typer.secho(
            tabulate(entries, headers, tablefmt="pretty"), fg=typer.colors.MAGENTA
        )
        raise typer.Exit()
    except OperationalError as err:
        typer.secho("Database operation error.", fg=typer.colors.RED, bold=True)
        typer.echo(err)
        raise typer.Exit(code=1)
