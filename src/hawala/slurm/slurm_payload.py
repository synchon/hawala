"""Define base class for generating SLURM scripts."""

import re
from typing import Optional

from ..abstract_context_adapter import AbstractContextAdapter
from ..exceptions import SLURMEmailValidationError, SLURMTimeValidationError


class SLURMPayload(AbstractContextAdapter):
    """
    Class for generating SLURM payloads.

    It providers a concrete implementation of ContextAdapter abstract base
    class.

    Parameters
    ----------
    job_name: str
        The job name to be used by SLURM.
    account: str
        The account to charge while submitting the job.
    time: str
        The wall clock time for the job, formatted as "HH:MM:SS".
    email_id: str
        The email address to send event notifications to.
    partition: str
        The partition to use for submitting jobs.
    redis_server_path: str
        The path to `redis-server` binary.
    redis_conf_path: str
        The path to redis configuration file.
    redis_password: str
        The password as specified in redis configuration file.
    python_path: str
        The path to `python` binary.
    python_script_path: str
        The path to python script.
    model_path: str
        The path to the model.
    morpheus_path: str
        The path to `morpheus` binary.
    model_db_path: str
        The path to the model database.
    mail_type: {"all", "begin", "end", "fail", "time_limit_90"}, optional
        The type of event for which to send mail to `email_id`
        (default "all").
    ntasks: int, optional
        The simultaneous tasks to launch. Useful for MPI applications
        (default 1).
    cpus_per_task: int, optional
        The processors per node to allocate for each task of `ntasks`
        (default 1).
    mem_per_cpu: int, optional
        The memory in MBs to allocate per node (default 2000).
    output_file: str, optional
        The file to contain stdout (default "`job_name`-<JOB_ID>.out")
    error_file: str, optional
        The file to contain stderr (default "`job_name`-<JOB_ID>.err")

    See Also
    --------
    ContextAdapter

    """

    def __init__(
        self,
        job_name: str,
        account: str,
        time: str,
        email_id: str,
        partition: str,
        redis_server_path: str,
        redis_conf_path: str,
        redis_password: str,
        python_path: str,
        python_script_path: str,
        model_path: str,
        morpheus_path: str,
        model_db_path: str,
        mail_type: Optional[str] = None,
        ntasks: Optional[int] = None,
        cpus_per_task: Optional[int] = None,
        mem_per_cpu: Optional[int] = None,
        output_file: Optional[str] = None,
        error_file: Optional[str] = None,
    ) -> None:
        """Initialize the class."""
        self.job_name = job_name
        self.account = account
        self.time = self._validate_time(time)
        self.email_id = self._validate_email(email_id)
        self.partition = partition
        self.redis_server_path = redis_server_path
        self.redis_conf_path = redis_conf_path
        self.redis_password = redis_password
        self.python_path = python_path
        self.python_script_path = python_script_path
        self.model_path = model_path
        self.morpheus_path = morpheus_path
        self.model_db_path = model_db_path
        self.mail_type = mail_type if mail_type else "all"
        self.ntasks = ntasks if ntasks else 1
        self.cpus_per_task = cpus_per_task if cpus_per_task else 1
        self.mem_per_cpu = mem_per_cpu if mem_per_cpu else 2000
        self.output_file = output_file if output_file else f"{job_name}-%j.out"
        self.error_file = error_file if error_file else f"{job_name}-%j.err"
        self._abc_redis_worker_time = self._convert_time_str_to_secs(self.time)

    def _validate_time(self, time: str) -> str:
        """
        Validate a given time string `time`.

        Parameters
        ----------
        time: str
            The time string to be verified.

        Returns
        -------
        str
            The verified time string.

        Raises
        ------
        SLURMTimeValidationError
            Raised if `time` is of incorrect format.

        """
        pattern = r"^[0-9]{2,3}:[0-5][0-9]:[0-5][0-9]$"

        if re.match(pattern, time):
            return time
        else:
            raise SLURMTimeValidationError(f'"{time}" is not a valid time string.')

    def _validate_email(self, email_id: str) -> str:
        """
        Validate a given email address `email_id`.

        Parameters
        ----------
        email_id: str
            The email address to be verified.

        Returns
        -------
        str
            The verified email address.

        Raises
        ------
        SLURMEmailValidationError
            Raised if `email_id` is of incorrect format.

        """
        pattern = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

        if re.match(pattern, email_id):
            return email_id
        else:
            raise SLURMEmailValidationError(
                f'"{email_id}" is not a valid email address.'
            )

    def _convert_time_str_to_secs(self, time: str) -> int:
        """
        Convert a `time` string to seconds.

        Parameters
        ----------
        time: str
            The time string to be converted.

        Returns
        -------
        int
            The integer obtained after conversion.

        """
        time_splitted = time.split(":")
        return (
            3600 * int(time_splitted[0])
            + 60 * int(time_splitted[1])
            + int(time_splitted[2])
        )

    def to_string(self) -> str:
        """Generate string representation of a SLURM job script."""
        return (
            f"#!/bin/bash \n"
            "\n"
            "# This SLURM job script is auto-generated by hawala. \n"
            "\n"
            f"#SBATCH --job-name={self.job_name} \n"
            f"#SBATCH --account={self.account} \n"
            f"#SBATCH --partition={self.partition} \n"
            f"#SBATCH --time={self.time} \n"
            f"#SBATCH --ntasks={self.ntasks} \n"
            f"#SBATCH --cpus-per-task={self.cpus_per_task} \n"
            f"#SBATCH --mem-per-cpu={self.mem_per_cpu} \n"
            f"#SBATCH --mail-type={self.mail_type} \n"
            f"#SBATCH --mail-user={self.email_id} \n"
            f"#SBATCH --output={self.output_file} \n"
            f"#SBATCH --error={self.error_file} \n"
            "\n"
            "# Retrieve IP of host \n"
            "HOSTNAME=$(hostname) \n"
            "HOST_IP=$(host ${HOSTNAME} | awk '{ print $4 }') \n"
            "printf 'HOST_IP=%s\n' ${HOST_IP} > hawala_run_metadata.log \n"
            "\n"
            "# Retrieve a random free port for Redis \n"
            "REDIS_PORT=$(comm -23 "
            "<(seq 49152 65535 | sort) "
            "<(ss -Htan | awk '{print $4}' | cut -d':' -f2 | sort -u) "
            "| shuf | head -n 1) \n"
            "printf 'REDIS_PORT=%s\n' ${REDIS_PORT} >> "
            "hawala_run_metadata.log \n"
            "\n"
            "# Initiate Redis in daemon mode \n"
            f"{self.redis_server_path} {self.redis_conf_path} "
            "--bind ${HOST_IP} "
            "--port ${REDIS_PORT} "
            "--daemonize yes \n"
            "\n"
            "# Wait for Redis to start as startup time depends on job load \n"
            "sleep 20 \n"
            "\n"
            "# Start pyABC Redis workers via srun so that `ntasks` is respected and"
            "# send it to background \n"
            "srun abc-redis-worker "
            "--host=${HOST_IP} "
            "--port=${REDIS_PORT} "
            f"--password={self.redis_password} "
            f"--runtime={self._abc_redis_worker_time}s "
            f"--processes={self.cpus_per_task} & \n"
            "\n"
            "# Submit Python script \n"
            f"{self.python_path} {self.python_script_path} "
            f"--model={self.model_path} "
            f"--db={self.model_db_path} "
            f"--morpheus={self.morpheus_path} "
            "--redis-ip=${HOST_IP} "
            "--redis-port=${REDIS_PORT} "
            f"--redis-password={self.redis_password} \n"
        )
