"""Define custom exceptions."""

from typing import Any


class HawalaError(Exception):
    """
    Base class for hawala errors.

    Parameters
    ----------
    message: str
        The message to display.

    Other Parameters
    ----------------
    **kwargs: Any
        The extra arguments are passed to the parent class.

    """

    def __init__(self, message: str, **kwargs: Any) -> None:
        super().__init__(message)


class HawalaConfValidationError(HawalaError):
    pass


class SLURMTimeValidationError(HawalaError):
    pass


class SLURMEmailValidationError(HawalaError):
    pass


class LocalTimeValidationError(HawalaError):
    pass
