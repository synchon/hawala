"""Define commmands for CLI application."""

from pathlib import Path

import sqlalchemy
import typer

from .db import database, metadata
from .local import app as local_app
from .settings import DATA_DIR_PATH, DB_PATH
from .slurm import app as slurm_app
from .utils import verify_config_integrity

app = typer.Typer()


@app.callback()
def callback():
    """
    Hawala for job submissions.
    """


@app.command()
def init():
    """
    Initialize a new hawala project.
    """
    # Check for existence of data directory; if not found, create it
    if not DATA_DIR_PATH.exists():
        DATA_DIR_PATH.mkdir(exist_ok=True)

    # Check for existence of storage database; if not found create
    if not DB_PATH.exists():
        DB_PATH.touch(exist_ok=True)
        # Create the database and necessary tables
        engine = sqlalchemy.create_engine(str(database.url))
        metadata.create_all(engine)

    typer.secho("Initialized a new hawala project.", fg=typer.colors.GREEN, bold=True)


@app.command()
def validate(
    config_file: Path = typer.Argument(
        ...,
        help="TOML config file for hawala.",
    ),
):
    """
    Validate hawala config file.
    """
    _, _, all_valid_paths = verify_config_integrity(config_file)
    raise typer.Exit(code=0 if all_valid_paths else 1)


app.add_typer(slurm_app, name="slurm")  # add SLURM
app.add_typer(local_app, name="local")  # add local
