"""Define tests for hawala/main.py"""

from typer.testing import CliRunner

from hawala.main import app

runner = CliRunner()


def test_callback() -> None:
    """Test callback."""
    result = runner.invoke(app, ["--help"])
    assert result.exit_code == 0
    assert "Hawala for job submissions." in result.stdout


def test_init() -> None:
    """Test `init` command invocation."""
    result = runner.invoke(app, ["init", "--help"])
    assert result.exit_code == 0
    assert "Initialize a new hawala project." in result.stdout


def test_validate() -> None:
    """Test `validate` command invocation."""
    result = runner.invoke(app, ["validate", "--help"])
    assert result.exit_code == 0
    assert "Validate hawala config file." in result.stdout


def test_validate_validation() -> None:
    """Test config file validation using `validate`."""
    result = runner.invoke(app, ["validate", "sample.toml"])
    assert result.exit_code == 1
    assert "Syntax and schema   : sample.toml ... OK" in result.stdout


def test_validate_validation_invalid_file() -> None:
    """Test invalid config file validation using `validate`."""
    result = runner.invoke(app, ["validate", "invalid_conf.toml"])
    assert result.exit_code == 1
    assert "Syntax and schema   : invalid_conf.toml ... FAIL" in result.stdout
