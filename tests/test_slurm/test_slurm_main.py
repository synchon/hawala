"""Define tests for hawala/slurm/main.py"""

from typer.testing import CliRunner

from hawala.slurm.main import app

runner = CliRunner()


def test_callback() -> None:
    """Test callback."""
    result = runner.invoke(app, ["--help"])
    assert result.exit_code == 0
    assert "Manage SLURM script generation and submission." in result.stdout


def test_slurm_submit() -> None:
    """Test `submit` sub-command of `slurm` command invocation."""
    result = runner.invoke(app, ["submit", "sample.toml"], input="n\n")
    assert result.exit_code == 1
    assert "This is not a hawala project yet." in result.stdout


def test_slurm_submissions() -> None:
    """Test `submissions` sub-command of `slurm` command invocation."""
    result = runner.invoke(app, ["submissions"])
    assert result.exit_code == 1
    assert "Database operation error." in result.stdout
