"""Define tests for hawala/utils.py"""

from pathlib import Path

import pytest
import typer

from hawala.exceptions import HawalaConfValidationError
from hawala.utils import (
    check_valid_paths,
    validate_hawala_toml,
    verify_config_integrity,
)


def test_validate_hawala_toml_pass() -> None:
    """Test valid hawala TOML against JSON schema."""
    config_path = Path(__file__).parent.absolute() / ".." / "sample.toml"
    _ = validate_hawala_toml(config_path)


def test_validate_hawala_toml_fail() -> None:
    """Test invalid hawala TOML validation against JSON schema."""
    config_path = Path(__file__).parent.absolute() / "invalid_conf.toml"
    with pytest.raises(HawalaConfValidationError):
        _ = validate_hawala_toml(config_path)


def test_validate_hawala_toml_invalid_file() -> None:
    """Test non-existent hawala TOML validation."""
    config_path = Path(__file__).parent.absolute() / "non_existent.toml"
    with pytest.raises(HawalaConfValidationError):
        _ = validate_hawala_toml(config_path)


def test_check_valid_paths() -> None:
    """Test valid path check in config."""
    sample_input = {
        "Redis configuration": "redis.conf",
        "Simulation script": "pyabc.py",
        "Model": "model.xml",
        "Morpheus binary": "morpheus",
    }
    sample_output = {
        "Redis configuration": ("redis.conf", False),
        "Simulation script": ("pyabc.py", False),
        "Model": ("model.xml", False),
        "Morpheus binary": ("morpheus", False),
    }
    verified_paths = check_valid_paths(sample_input)
    assert verified_paths == sample_output


def test_verify_config_integrity() -> None:
    """Test config integrity verification."""
    config_path = Path(__file__).parent.absolute() / ".." / "sample.toml"
    _, _, all_valid_paths = verify_config_integrity(config_path)
    assert all_valid_paths is False


def test_verify_config_integrity_invalid_file() -> None:
    """Test config integrity verification of non-existent file."""
    config_path = Path(__file__).parent.absolute() / ".." / "non_existent.toml"
    with pytest.raises(typer.Exit):
        _, _, all_valid_paths = verify_config_integrity(config_path)
        assert all_valid_paths is False
