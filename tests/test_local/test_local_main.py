"""Define tests for hawala/local/main.py"""

from typer.testing import CliRunner

from hawala.local.main import app

runner = CliRunner()


def test_callback() -> None:
    """Test callback."""
    result = runner.invoke(app, ["--help"])
    assert result.exit_code == 0
    assert "Manage local script generation and submission." in result.stdout


def test_local_submit() -> None:
    """Test `submit` sub-command of `local` command invocation."""
    result = runner.invoke(app, ["submit", "sample.toml"], input="n\n")
    assert result.exit_code == 1
    assert "This is not a hawala project yet." in result.stdout


def test_local_submissions() -> None:
    """Test `submissions` sub-command of `local` command invocation."""
    result = runner.invoke(app, ["submissions"])
    assert result.exit_code == 1
    assert "Database operation error." in result.stdout
