# Hawala

Submit FitMultiCell jobs to HPC with trust and peace.

## About

_Hawala_ is a system of money exchange originating in present day India. It is considered to be the first of its kind and has been in existence since 8th century. This is the precursor to the present day banking as we know it. The system is rooted in trust between people carrying out the transaction facilitated by agents or _hawaladar_. The interesting part is that there is no actual transfer of money but rather transfer of debt.

Much like the actual system, this tool also named `hawala`, aims to abstract the part of actually writing job scripts and inference scripts and expose it via a configuration file. You can thus focus more on your inference and fiddle less with the painful part of writing scripts. Similar to the actual system, every transaction carried out is stored as a record in a modern day ledger or database. `hawala` knows how to carry out the transaction and in case of any issue, will let you know the reason.

## Usage

This is a __developmental__ project and thus expect breaking changes.

The only way to interact with `hawala` is via a TOML file. You specify your options in the file and the tool converts it to the corresponding job script. A sample file can be like:

```toml

# A sample configuration for hawala.

title = "Hawala example conf file" # a short title to describe which pipeline is being run

[slurm]                            # block describing parameters for SLURM
job_name = "hawala_test"           # SLURM job name
account = "test_acc"               # SLURM account to assign job to
time = "24:42:24"                  # wall clock time for SLURM resource allocation
email_id = "abc@xyz.com"           # email address for SLURM notification
partition = "haswell"              # SLURM partition to use
mail_type = "all"                  # optional (defaults to "all")
ntasks = 1                         # optional (defaults to 1)
cpus_per_task = 4                  # optional (default to 1)
mem_per_cpu = 2500                 # optional (default to 2000)
output_file = "output.out"         # optional (defaults to <job_name>-<job_id>.out)
error_file = "error.err"           # optional (defaults to <job_name>-<job_id>.err)

[local]                            # block describing parameters for local execution
job_name = "hawala_test_local"     # local job name (used for naming generated scripts)
os = "linux"                       # platform to run on
time = "24:42:24"                  # wall clock time for local resource allocation
cpus = 2                           # no. of cpu cores to use for sampling, optional (default 1)
redis_cli_path = "redis-cli"       # path to binary of redis-cli (required for job control)

[redis]                            # block describing parameters for Redis
server_path = "redis-server"       # path to binary of redis-server
conf_path = "redis_6379.conf"      # path to conf of redis-server
password = "redissider"            # password of the server (always keep it safe and thus enforced here)

[pyabc]                            # block describing parameters for pyABC
python_bin = "python"              # path to binary of python
python_script = "pyabc.py"         # path to pyABC inference script
model_path = "model.xml"           # path to simulation model
morpheus_bin = "morpheus"          # path to binary of Morpheus
database_path = "hawala_test.db"   # path to the simulation database
```

You can also copy and modify the `sample.toml` in the root of the repository. To get an overview of the tool, try:

```
$ hawala --help
```

### Initiating a project

`hawala` works in a per-project basis thus, you need to first create a directory containing all your data files like Morpheus model, pyABC inference script, Redis configuration file and the `hawala` configuration file. Then, you can initiate a new project like so:

```
$ hawala init
```

### Validation of configuration file

Once you have your project initiated, you can validate the `hawala` TOML like so:

```
$ hawala validate "<your-toml-file>"
```

Internally, it validates against a defined schema. You will see error if you have corrupt TOML, invalid types for the options and/or missing required values.

You can also use the `validate` command without a project.

### Submission to job schedulers

For submission to job schedulers, you need to have a project set up before moving forward.

#### SLURM

For now, `hawala` only supports job submissions to the [SLURM](https://slurm.schedmd.com/) job scheduler, like so:

```
$ hawala slurm submit "<your-toml-file>"
```

Before submission, this will validate the file and also display the generated job script. You will then an option to submit or abort it.

### Running on the local system

You can also try out a simulation on your local system before submitting to a HPC cluster or you can just use it for a quick simulation. You can use it like so:

```
$ hawala local submit "<your-toml-file>"
```

### Viewing past submissions

To get a record of the past job submissions for a project, you can use:

```
$ hawala slurm submissions
```

for SLURM submissions and:

```
$ hawala local submissions
```

for local submissions. This will display a pretty-printed table of the records to your console.

## Installation

Use pip to install `hawala` (recommended inside a virtualenv):

```
$ pip install hawala --extra-index-url https://__token__:<your_personal_token>@gitlab.com/api/v4/projects/22880494/packages/pypi/simple
```

## Development

In case you want to develop, clone the repository and make sure you have [poetry](https://python-poetry.org/docs/) installed before proceeding. After installing poetry, use the following command to set up everything inside the cloned repository directory:

```
$ poetry install
```

### Unit Testing

`pytest` is used for running unit tests and is run via `tox`, like so:

```
$ poetry run tox -e test
```

### Code Formatting and Linting

`black` is used for code formatting and `isort` is used for sorting imports. `pycodestyle` is used for code linting, and everything is run together via `tox`, like so:

```
$ poetry run tox -e lint
```

### Static Type Checking

`mypy` is used for static type check and is run via `tox`, like so:

```
$ poetry run tox -e type-check
```
